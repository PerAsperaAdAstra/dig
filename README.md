# Python Dynamic Interactive Gantt


## Description
This is a Python-based Dynamic, Interactive Gantt tool (DIG).  It's intended to support the creation, modification, and exploration of schedule information in Gantt chart form.

## Installation
DIG requires Python modules/libraries, which are hopefully part of most basic installations.  They are:
* json
* tkinter
* datetime

## Usage
To use, run the python script dynamic_interactive_gantt.py.

You can then either start a schedule by clicking 'Add Item' or you can load an existing schedule with File->Open.

To save a schedule (or any changes to a schedule) use File-Save.

You can click a schedule item to show more details about it and to edit its parameters (which will be applied if you select the 'Save and Close' button).
    * Note that this does not save the new information to the JSON file-- you must still use File->Same

The tool expects a JSON-formatted schedule like the example shown below.

{ "task 1": 
    { 
	"task_id":1,
	"start_date":"01/01/2023",
	"duration_calendar_days":40,
	"prerequisites":[]
	},
 "task 2": 
    { 
	"task_id":2,
	"start_date":"04/01/2023",
	"duration_calendar_days":25,
	"prerequisites":["task 1"]
	}
}



## TODO
Lots of stuff to do, including (in no particular order):
* better tools to create schedule items
* ability to zoom in and out
* ability to calculate critical path
* support for task predecessors/dependencies
* "unit test" schedule JSONs for errors/failures/unexpected things
* general beautification
* fix scrollwheel use
* export chart as an image
* better labelling

## Contributing and Support
Feel free to provide comments/criticism/pull requests/feature requests/etc.

## License
See license file.

## Project status
Still in development.
