#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
dynamic_interactive_gantt.py

This program is intended to provide a graphical
way to create, edit, and explore schedule information
presented as a Gantt chart

'''

###################################
#IMPORT MODULES
###################################
import json
import tkinter as tk
import datetime
from tkinter import filedialog
from tkinter import messagebox


###################################
#PRIMARY CLASS
###################################
class GanttChart(tk.Frame):
    def __init__(self, parent):

        tk.Frame.__init__(self, parent)

        # add menu bar
        menu_bar = tk.Menu(root)
        file_menu = tk.Menu(menu_bar, tearoff=0)
        menu_bar.add_cascade(label="File", menu=file_menu)
        file_menu.add_command(label="Open", command=self.open_file)
        file_menu.add_command(label="Save", command=self.save_file)

        #tool_menu = tk.Menu(menu_bar, tearoff=1)  #TODO get functionality working first
        #menu_bar.add_cascade(label="Tools", menu=tool_menu)
        #tool_menu.add_command(label="Save as Image", command=self.save_image)

        help_menu = tk.Menu(menu_bar, tearoff=1)
        menu_bar.add_cascade(label="Help", menu=help_menu)
        help_menu.add_command(label="About", command=self.draw_about)

        root.config(menu=menu_bar)

        # define constants
        self.font_size=20
        self.button_height=0 #px
        self.button_color="sky blue"
        self.background_color="gray22"
        self.label_font_color="gray80"
        self.pixels_per_days=20
        self.schedule_data_dict={}

        # create the canvas widget
        self.canvas = tk.Canvas(self, borderwidth=0, background=self.background_color)
        self.gantt_frame = tk.Frame(self.canvas, background=self.background_color)
        self.vsb = tk.Scrollbar(self, orient="vertical", command=self.canvas.yview)
        self.hsb = tk.Scrollbar(self, orient="horizontal", command=self.canvas.xview)
        self.canvas.configure(yscrollcommand=self.vsb.set, xscrollcommand=self.hsb.set)

        self.vsb.pack(side="right", fill="y")
        self.hsb.pack(side="bottom", fill="x")

        self.canvas.pack(side="left", fill="both", expand=True)
        self.canvas.create_window((4,4), window=self.gantt_frame, anchor="nw", tags="self.gantt_frame")

        self.gantt_frame.bind("<Configure>", self.onFrameConfigure)

        self.draw_gantt()

    def onFrameConfigure(self, event):
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))

    def open_file(self): #open a JSON that contains the info needed for a gantt chart
        filepath = filedialog.askopenfilename(title="Select a Schedule JSON",
            filetypes=(("JSON", "*.json"), ("All files", "*.*")), defaultextension=".json")
        if len(filepath) < 1:
            print('no file selected')
            return

        print("\n\n Loading :", filepath)
        with open(filepath) as json_file:
            self.schedule_data_dict = json.load(json_file)
        print(self.schedule_data_dict)
        self.draw_gantt()

    def save_file(self): #save a JSON that contains the info needed for a gantt chart
        #TODO add a sort of "unit test" for the file here and alert the user if there are failures/discrepancies

        filepath = filedialog.asksaveasfilename(title="Enter a file name and location",
            filetypes=(("JSON", "*.json"), ("All files", "*.*")), defaultextension=".json")
        if len(filepath) < 1:
            print('no name provided')
            return

        print("\n\n Saving :", filepath)
        with open(filepath, "w") as json_file:
            json.dump(self.schedule_data_dict, json_file, indent=2)
        print(self.schedule_data_dict)

    def _on_mousewheel(self, event): # scroll the canvas up/down when the mousewheel is used

        if event.num == 5 or event.delta < 0:
            self.figure.yview_scroll(1, "units")
        elif event.num == 4 or event.delta > 0:
            self.figure.yview_scroll(-1, "units")


    def draw_gantt(self): #draw the gantt chart

        self.clear_screen()

        if len(self.schedule_data_dict) < 1:
            button = tk.Button(self.gantt_frame, text="Add Item", command=lambda:self.draw_gantt_item(""), bg=self.button_color, height=self.button_height, font=("Helvetica", self.font_size))
            button.grid()

        else:
            list_of_task_names = list(self.schedule_data_dict.keys())

            # find the first date and the last date
            first_date = 99999999999999999999999999
            end_date = 0
            for task_name in list_of_task_names:
                start_date = self.schedule_data_dict[task_name]['start_date']
                month, day, year = map(int, start_date.split('/'))
                # create a date object
                my_date = datetime.date(year, month, day)
                the_ordinal = my_date.toordinal()
                if the_ordinal < first_date: first_date = the_ordinal
                current_end_date = the_ordinal+self.schedule_data_dict[task_name]['duration_calendar_days']
                if current_end_date > end_date: end_date = current_end_date


            #loop through the dict to draw
            list_of_task_names = list(self.schedule_data_dict.keys())
            the_row = 0
            for task_name in list_of_task_names:
                start_date = self.schedule_data_dict[task_name]['start_date']
                duration = self.schedule_data_dict[task_name]['duration_calendar_days']

                #calculate column start
                start_date = self.schedule_data_dict[task_name]['start_date']
                month, day, year = map(int, start_date.split('/'))
                # create a date object
                my_date = datetime.date(year, month, day)
                the_ordinal = my_date.toordinal()
                the_column = the_ordinal-first_date

                button = tk.Button(self.gantt_frame, text=task_name, command=lambda:self.draw_gantt_item(task_name), bg=self.button_color, height=self.button_height, font=("Helvetica", self.font_size))
                button.grid(row=the_row, column=the_column, columnspan=duration, sticky="EW") #fill the columnspan
                the_row+=1


            button = tk.Button(self.gantt_frame, text="Add Item", command=lambda:self.draw_gantt_item(""), bg=self.button_color, height=self.button_height, font=("Helvetica", self.font_size))
            button.grid(columnspan=8)
            the_row+=1

            #create dates on the bottom (days)
            for n in range(0, end_date-first_date):
                the_label = tk.Label(self.gantt_frame, text=str(n), background=self.background_color, fg=self.label_font_color)
                the_label.grid(row=the_row, column=n)
                
                if n%5 == 0:
                    reconstructed_ordinal = n+first_date
                    reconstructed_date = datetime.date.fromordinal(reconstructed_ordinal)
                    the_label = tk.Label(self.gantt_frame, text=reconstructed_date.strftime('%m/%d/%Y'), background=self.background_color, fg=self.label_font_color)
                    the_label.grid(row=the_row+1, column=n, columnspan=4)

    def draw_gantt_item(self, task_name):
    
        def save_and_close():
            new_dict = {task_name_entry.get():{"start_date": start_date_entry.get(), "duration_calendar_days": int(duration_entry.get()), "prerequisites":[], "task_id":999}}
            self.schedule_data_dict.update(new_dict)
            print(self.schedule_data_dict)
            self.close_gantt_item_window()

    
        if len(task_name) < 1:
            task_start_date = ""
            task_duration = 1
        else: 
            task_start_date = self.schedule_data_dict[task_name]["start_date"]
            task_duration = self.schedule_data_dict[task_name]["duration_calendar_days"]

        self.popup_window = tk.Toplevel()
        self.popup_window.title("Task Editor")
        self.popup_window.configure(bg=self.background_color)

        the_row = 0
        self.popup_label = tk.Label(self.popup_window, text="Use the below fields to update the task information.")
        self.popup_label.grid(row=the_row, columnspan=2, sticky='ew')

        # Task name label and entry
        the_row+=1
        tk.Label(self.popup_window, text="Task Name").grid(row=the_row, column=0)
        task_name_entry = tk.Entry(self.popup_window)
        task_name_entry.insert(0, task_name)
        task_name_entry.grid(row=the_row, column=1)

        # Start date label and entry
        the_row+=1
        tk.Label(self.popup_window, text="Start Date").grid(row=the_row, column=0)
        start_date_entry = tk.Entry(self.popup_window)
        start_date_entry.insert(0, task_start_date)
        start_date_entry.grid(row=the_row, column=1)
        
        # Duration label and entry
        the_row+=1
        tk.Label(self.popup_window, text="Duration (days)").grid(row=the_row, column=0)
        duration_entry = tk.Entry(self.popup_window)
        duration_entry.insert(0, task_duration)
        duration_entry.grid(row=the_row, column=1)
        
        the_row+=1
        self.button = tk.Button(self.popup_window, text='Save and Close', command=save_and_close, bg=self.button_color, height=self.button_height, font=("Helvetica", self.font_size))
        self.button.grid(row=the_row, column=0)

        self.button = tk.Button(self.popup_window, text='Close without Saving', command=self.close_gantt_item_window, bg=self.button_color, height=self.button_height, font=("Helvetica", self.font_size))
        self.button.grid(row=the_row, column=1)
        

    def draw_about(self):
        messagebox.showinfo("About", "Check out the README, yo.")


    def close_gantt_item_window(self):
        self.popup_window.destroy()
        self.draw_gantt()

    '''
    TODO: figure out how to capture things off-screen
    def save_image(self): # Function to save the frame as an image
        from PIL import ImageGrab
        
        widget = self.gantt_frame
        # Get the coordinates of the widget
        x = root.winfo_rootx() + widget.winfo_x()
        y = root.winfo_rooty() + widget.winfo_y()
        x1 = x + widget.winfo_width()
        y1 = y + widget.winfo_height()

        # Take a screenshot of the widget
        image = ImageGrab.grab((x, y, x1, y1))

        # Save the screenshot as an image file
        file_name = "frame_image.png"
        image.save(file_name)
    '''



    def clear_screen(self):

        _list = self.master.winfo_children()

        for item in _list :
            if item.winfo_children() :
                _list.extend(item.winfo_children())

        for item in _list:
            if 'scrollbar' not in item.winfo_name() and 'canvas' not in item.winfo_name() and 'menu' not in item.winfo_name() and 'ganttchart' not in item.winfo_name() and 'frame' not in item.winfo_name():
                try: item.grid_forget()
                except: print('failed to forget: '+ str(item))



###################################
#MAIN CODE
###################################
if __name__ == '__main__':
    # create the main window
    root = tk.Tk()
    root.title("Dynamic Interactive Gantt")
    root.geometry("1100x800")

    # create the scrollable figure widget and pack it into the main window
    figure_widget = GanttChart(root)
    figure_widget.pack(side="top", fill="both", expand=True)

    # start the main event loop
    root.mainloop()


